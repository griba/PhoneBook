@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div id="app">
                <app :profile="{{ Auth::user()->details->toJson() }}"></app>
            </div>
        </div>
    </div>
</div>
@endsection
