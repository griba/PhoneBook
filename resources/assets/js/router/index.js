import Vue from 'vue'
import VueRouter from 'vue-router'

import Users from '../views/Users.vue'
import Favourites from '../views/Favourites.vue'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	routes: [
            { path: '/home', redirect: '/home/users' },
            { path: '/home/users', name: 'users', component: Users, props: true },
            { path: '/home/favourites', name: 'favourites', component: Favourites, props: true },
	]
})

export default router