<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'auth'],function(){
    Route::get('/home{any}', 'HomeController@index')->name('home')->where('any', '.*');
    Route::resource(
            'api/users', 
            'UserDetailsController',
            [
                'except' => [ 'create', 'edit' ], 
                'parameters' => [ 'users' => 'userDetails' ],
            ]);
    Route::resource(
            'api/favourites', 
            'UserFavouriteController',
            [
                'only' => [ 'index', 'store', 'destroy' ], 
                'parameters' => [ 'favourites' => 'userFavourite' ],
            ]);

    Route::get('/users/export', 'UserDetailsController@export')->name('export');
});