<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserFavouritesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);
        return [
            'user_id' => $this->favouriteDetails->user_id,
            'first_name' => $this->favouriteDetails->first_name,
            'last_name' => $this->favouriteDetails->last_name,
            'phone' => $this->favouriteDetails->phone,
            'favourite' => true,
            'favourite_id' => $this->id,
        ];
    }
}
