<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Models\User;
use App\Models\UserDetails;
use App\Http\Resources\UserDetailsResource;
use App\Http\Requests;
use Illuminate\Support\Facades\Gate;

class UserDetailsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Requests\UsersListRequest $request)
    {
        $users = UserDetails::with('favourite')
                ->where(function($query) use ($request) {
            if($request->filled('first_name'))
                $query->where('first_name',$request->first_name);
            
            if($request->filled('last_name'))
                $query->where('last_name',$request->last_name);
            
            if($request->filled('phone'))
                $query->where('phone', \App\Classes\PhoneNumber::clear($request->phone));
        });
        if($request->filled('sort_column') && $request->filled('sort_order')) {
            $users->orderBy($request->sort_column, $request->sort_order);
        }
        return UserDetailsResource::collection($users->paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\UserDetailsStoreRequest $request)
    {
        try {
            if (Gate::allows('create', UserDetails::class)) {
                $user = User::create([
                    'email' => $request->email,
                    'password' => Hash::make($request->password),
                ]);
                $userDetails = $user->details()->create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'phone' => $request->phone,
                ]);
                return response()->json($userDetails,201);
            } else {
                return response()->json([],405);
            }
        } catch (\Exception $e) {
            return response()->json([],500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  UserDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function show(UserDetails $userDetails)
    {
        try {
            return response()->json($userDetails, 200);
        } catch (\Exception $e) {
            return response()->json([],500);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  UserDetails  $userDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Requests\UserDetailsUpdateRequest $request, UserDetails $userDetails)
    {
        try {
            if (Gate::allows('update', $userDetails)) {
                DB::beginTransaction();
                $userDetails->fill($request->all());
                $userDetails->save();

                DB::commit();
                return response()->json([],200);
            } else {
                return response()->json([],405);
            }
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([],500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  UserDetails $userDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserDetails $userDetails)
    {
        try {
            if (Gate::allows('delete', $userDetails)) {
                $userDetails->user()->delete(); 

                return response()->json([],200);
            } else {
                return response()->json([],405);
            }
        } catch (\Exception $e) {
            return response()->json([],500);
        }
    }
    
    public function export(Requests\UserDetailsExportRequest $request) {
        try {
            $users = UserDetails::whereIn('user_id',$request->users)->get();
            $data = compact('users');
            
            switch($request->format) {
                case 'xlsx':
                    return \Excel::create('contacts', function($excel) use ($data) {
                        
                        $excel->sheet('Контакты', function($sheet) use ($data) {
                            $sheet->setColumnFormat(array(
                                'D' => '@',
                            ));
                            $sheet->loadView('export.xlsx', $data);
                        });
                    })->export('xlsx');
                    break;
                default: // PDF
                    $pdf = \PDF::loadView('export.pdf', $data);
                    $pdf->setPaper('a4', 'landscape');
                    return $pdf->stream();
            }
        } catch (\Exception $e) {
            return redirect('home');
        }
    }

}
