<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserFavourite extends Model
{
    protected $table = 'users_favourites';
    public $timestamps = false;
      
    protected $fillable = [
        'user_id', 'favourite_user_id'
    ];
        
    public function user() {
        return $this->belongsTo('App\Models\User','user_id','id');
    }
    public function userDetails() {
        return $this->belongsTo('App\Models\UserDetails','user_id','user_id');
    }
    
    public function favourite() {
        return $this->belongsTo('App\Models\User','favourite_user_id','id');
    }
    public function favouriteDetails() {
        return $this->belongsTo('App\Models\UserDetails','favourite_user_id','user_id');
    }
}
