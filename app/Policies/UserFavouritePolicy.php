<?php

namespace App\Policies;

use App\Models\User;
use App\Models\UserFavourite;
use Illuminate\Auth\Access\HandlesAuthorization;
use App\Services\AgentRelationsService;

class UserFavouritePolicy
{
    use HandlesAuthorization;
    
    public function create(User $user) {
        return true;
    }
    
    public function view(User $user, UserFavourite $favourite)
    {
        return $user->id == $favourite->user_id;
    }
    
    public function update(User $user, UserFavourite $favourite)
    {
        return $user->id == $favourite->user_id;
    }

    public function delete(User $user, UserFavourite $favourite)
    {
        return $user->id == $favourite->user_id;
    }
}
