<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeysUsersFavouriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /*
        Schema::disableForeignKeyConstraints();
        Schema::table('users_favourites', function (Blueprint $table) {
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
            
            $table->foreign('favourite_user_id')
                  ->references('id')->on('users')
                  ->onDelete('cascade');
        });
        //*/
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        /*
        Schema::table('users_favourites', function (Blueprint $table) {
            $table->dropForeign(['user_id','favourite_user_id']);
        });
        //*/
    }
}
